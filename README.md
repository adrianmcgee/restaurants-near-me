![Restaurant Logo](Documents/Images/logo.jpg)

Restaurants is a Zomato Search and Favouriting tool written in Swift. It returns the Top 10 rated Restaurants based on your search criteria.
## Table of Contents

- [Getting Started](#getting-started)
  * [Environment setup](#environment-setup)
  * [Project setup](#project-setup)
- [Development Environment](#development-environment)
- [Architecture](#architecture)
- [Todos](#todos)

## Getting Started

### Environment setup

-   macOS Version: Sierra 10.12.x
-   Xcode Version: 10.x (Use branch `tech/xcode941_compatibility` for Xcode 9.4.1 compatible version)
-   Swift Version: 4.0

### Project setup

**1. Clone**

Clone the repository from the appropriate URL

```bash
git clone git@bitbucket.org:adrianmcgee/restaurants-near-me.git
```

**2. Dependencies**

Our 3rd party dependencies are managed using _Carthage_.
Run the following command to checkout the project's dependencies:

```bash
carthage bootstrap --platform iOS
```

### Development Environment

-   [Carthage](https://github.com/Carthage/Carthage) - package management for Cocoa applications

## Architecture

The following concepts are used throughout the codebase:

-   [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel)

## Todos
- Add Unit Tests
- Expand Search functionality to be able to incorporate Geolocation, custom Sorting and Filters
- Improve UX/UI
