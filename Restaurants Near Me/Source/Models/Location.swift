//
//  Location.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import SwiftyJSON

public struct Location {

    public let address: String?
    public let locality: String?
    public let city: String?
    public let city_id: Int?
    public let latitude: String?
    public let longitude: String?
    public let postcode: String?
    public let countryId: Int?

}

extension Location {

    init?(json: JSON) {
        address = json["address"].string
        locality = json["locality"].string
        city = json["city"].string
        city_id = json["city_id"].int
        latitude = json["latitude"].string
        longitude = json["longitude"].string
        postcode = json["zipcode"].string
        countryId = json["countryId"].int
    }

}
