//
//  Restaurant.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import SwiftyJSON

public struct Restaurant {

    public let id: String?
    public let name: String?
    public let urlPath: String?
    public let imageURLPath: String?
    public let location: Location?
    public let rating: Rating?

}

extension Restaurant {
    init?(json: JSON) {
        id = json["id"].string
        location = Location(json: json["location"])
        rating = Rating(json: json["user_rating"])
        urlPath = json["url"].string
        imageURLPath = json["featured_image"].string
        name = json["name"].string
    }

}
