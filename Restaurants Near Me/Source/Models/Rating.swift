//
//  Rating.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import SwiftyJSON

public struct Rating {

    public let ratingValue: Int?
    public let ratingText: String?

}

extension Rating {

    init?(json: JSON) {
       ratingValue = json["aggregate_rating"].int
       ratingText = json["rating_text"].string
    }

}
