//
//  AppStartRouter.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import UIKit

final class AppStartRouter: AppStartRouterType {

    init(window: UIWindow?) {
        self.window = window
    }

    // MARK: - Private

    // MARK: Properties

    private weak var window: UIWindow?

    // MARK: - Public

    // MARK: Functions

    func presentInitialScreen() {
        
        let storyboard = UIStoryboard.init(name: "RestaurantStoryboard", bundle: nil)

        let tabBar = storyboard.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController

        let searchViewController: SearchViewController = storyboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchViewController
        let router = SearchRouter()
        let httpClient = RestaurantClient()
        let viewModel = SearchViewModel(client: httpClient, searchRouter: router)

        searchViewController.viewModel = viewModel

        let favouritesViewController: FavouritesViewController = storyboard.instantiateViewController(withIdentifier: "FavouritesVC") as! FavouritesViewController
        let favouritesRouter = FavouritesRouter()
        let favouritesHttpClient = RestaurantClient()
        let favouritesViewModel = FavouritesViewModel(client: favouritesHttpClient, favouritesRouter: favouritesRouter)

        favouritesViewController.viewModel = favouritesViewModel

        tabBar.setViewControllers([searchViewController, favouritesViewController], animated: true)

        window?.rootViewController = tabBar
        window?.makeKeyAndVisible()
    }

}
