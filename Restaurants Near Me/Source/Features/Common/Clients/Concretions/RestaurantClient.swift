//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 30/9/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import Haneke
import Result
import SwiftyJSON

public final class RestaurantClient: RestaurantClientType {

    // MARK: - SearchClientType conformance

    public func search(keyword: String?, completion: @escaping (Result<SearchResponse, HTTPError>) -> Void) {

        let httpRequest = HTTPRequestBuilder(apiVersionKey: "user-key", apiVersion: "38e851a65a766075e758d9803c21faac")
                .addPath("search?")
                .addQueryParameter(key: "q", value: keyword!)
                .addQueryParameter(key: "sort", value: "rating")
                .addQueryParameter(key: "order", value: "desc")
                .addMethod(.get)
                .build()

        // HTTP Client
        let httpClient = HTTPClient(with: "https://developers.zomato.com/api/v2.1")

        httpClient.request(httpRequest, response: { httpResponse in

            let cache = Shared.dataCache
            switch httpResponse {
            case .success((let data)):
                var restaurants: [Restaurant] = []

                let json = JSON(data: data)
                cache.set(value: data, key: httpRequest.urlPath)

                if let result = json["restaurants"].array {
                    for item in result {
                        if let rest = Restaurant(json: item["restaurant"]) {
                            restaurants.append(rest)
                        }
                    }
                    completion(Result(value: (restaurants)))
                }

            case .failure(let error):
                switch error {
                case .client, .server, .request, .unknown:
                    completion(Result(error: error))

                case .noNetwork:
                    var restaurants: [Restaurant] = []
                    cache.fetch(key: httpRequest.urlPath).onSuccess { cachedData in
                                let json = JSON(data: cachedData)

                                if let result = json["restaurants"].array {
                                    for item in result {
                                        if let rest = Restaurant(json: item["restaurant"]) {
                                            restaurants.append(rest)
                                        }
                                    }

                                    completion(Result(value: (restaurants)))
                                }
                            }.onFailure { _ in
                                completion(Result(error: error))
                            }
                }

            }

        })

    }

    public func findRestaurantWith(id: String?, completion: @escaping (Result<FindRestaurantResponse, HTTPError>) -> Void) {

        let httpRequest = HTTPRequestBuilder(apiVersionKey: "user-key", apiVersion: "38e851a65a766075e758d9803c21faac")
                .addPath("restaurant?")
                .addQueryParameter(key: "res_id", value: id!)
                .addMethod(.get)
                .build()

        // HTTP Client
        let httpClient = HTTPClient(with: "https://developers.zomato.com/api/v2.1")

        httpClient.request(httpRequest, response: { httpResponse in

            let cache = Shared.dataCache
            switch httpResponse {
            case .success((let data)):

                let json = JSON(data: data)
                cache.set(value: data, key: httpRequest.urlPath)

                if let restaurant = Restaurant(json: json) {
                    //TODO This delay was added as a temporary fix to allow all restaurants to be loaded
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        completion(Result(value: (restaurant)))
                    })

                }

            case .failure(let error):
                switch error {
                case .client, .server, .request, .unknown:
                    completion(Result(error: error))

                case .noNetwork:
                    cache.fetch(key: httpRequest.urlPath).onSuccess { cachedData in
                        let json = JSON(data: cachedData)

                        if let restaurant = Restaurant(json: json) {
                            completion(Result(value: (restaurant)))
                        }
                    }.onFailure { _ in
                        completion(Result(error: error))
                    }
                }

            }

        })

    }

}

public enum SearchParameter: String {
    case entity_id
    case entity_type
    case keyword = "q"
    case resultsOffset = "start"
    case count
    case lat
    case lon
    case cuisines
    case establishment_type
    case collection_id
    case category
    case sort
    case sortingOrder = "order"
    
    var name: String {
        return self.rawValue
    }
}

public enum EntityType: String {
    case city
    case subzone
    case zone
    case landmark
    case metro
    case group
}
