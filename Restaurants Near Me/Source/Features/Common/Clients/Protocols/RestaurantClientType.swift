//
//  SearchClientType.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 30/9/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Result

public typealias SearchResponse = ([Restaurant])
public typealias FindRestaurantResponse = (Restaurant)

public protocol RestaurantClientType {

    func search(keyword: String?, completion: @escaping (Result<SearchResponse, HTTPError>) -> Void)
    func findRestaurantWith(id: String?, completion: @escaping (Result<FindRestaurantResponse, HTTPError>) -> Void)

}
