//
//  SearchViewModel.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 30/9/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import Result
import SwiftyJSON

final class SearchViewModel: SearchViewModelType {

    init(client: RestaurantClientType,
         searchRouter: SearchRouterType) {

    self.restaurantClient = client
    self.router = searchRouter
    self.previousSearchString = ""
    }

    // MARK: - SearchViewModelType conformance

    var errorTitle: String {
        return "Error"
    }

    var errorMessage: String {
        return "There was an error. Please try again later"
    }

    var errorDismissTitle: String {
        return "OK"
    }

    var loadingSpinnerTitle: String {
        return "LOADING RESTAURANTS"
    }

    var cellIdentifier: String {
        return "SearchTableViewCell"
    }

    var cellHeight: Float {
        return 200
    }

    var restaurants: [Restaurant] = []
    var allRestaurants: [SearchDisplayModelType] = []
    var previousSearchString: String

    func searchRestaurantsWith(keyword: String?, completion: @escaping (Result<Void, HTTPError>) -> Void) {

        guard let keyword = keyword, !keyword.isEmpty else {
            return
        }

        let formattedKeyword = keyword.replacingOccurrences(of: " ", with: "")

        restaurantClient.search(keyword: formattedKeyword ) { [unowned self] result in

            switch result {
            case .success(let restaurantsResponse):
                self.updateContent(restaurantsResponse)
                completion(Result(value: ()))

            case .failure(let error):
                completion(Result(error: error))
            }

        }
        
    }

    func numberOfRestaurants() -> Int {
        return allRestaurants.count > 10 ? 10 : restaurants.count
    }

    func shouldSearchWith(searchString: String) -> Bool {
        if searchString == previousSearchString || searchString.isEmpty {
            return false
        }
        else {
            previousSearchString = searchString
            return true
        }
    }

    // MARK: - Private

    private let restaurantClient: RestaurantClientType
    private let router: SearchRouterType

    private func updateContent(_ searchResponse: SearchResponse) {
        persistRestaurants(searchResponse)
        allRestaurants = searchResponse.compactMap { SearchDisplayModel(restaurant: $0) }
    }

    private func persistRestaurants(_ searchResponse: SearchResponse) {
        restaurants = searchResponse
    }

}
