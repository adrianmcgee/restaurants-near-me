//
//  SearchViewModelType.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 30/9/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import Result

protocol SearchViewModelType {
    var restaurants: [Restaurant] { get }
    var allRestaurants: [SearchDisplayModelType] { get }
    var errorTitle: String { get }
    var errorMessage: String { get }
    var errorDismissTitle: String { get }
    var loadingSpinnerTitle: String { get }
    var cellIdentifier: String { get }
    var cellHeight: Float { get }
    var previousSearchString: String { get set }

    func searchRestaurantsWith(keyword: String?, completion: @escaping (Result<Void, HTTPError>) -> Void)
    func numberOfRestaurants() -> Int
    func shouldSearchWith(searchString: String) -> Bool
}
