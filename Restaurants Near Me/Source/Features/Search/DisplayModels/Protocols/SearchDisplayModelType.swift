//
//  SearchDisplayModelType.swift
//  Restaurants Near Me
//
//  Created by Adrian McGee on 10/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import UIKit

protocol SearchDisplayModelType {

    var id: String? { get }

    var title: String { get }

    var imageURL: URL? { get }

    var url: URL? { get }

    var address: String { get }

    var placeholderImage: UIImage { get }

    }
