//
//  SearchTableViewCell.swift
//  Restaurants Near Me
//
//  Created by Adrian McGee on 10/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import AlamofireImage
import UIKit

class SearchTableViewCell: UITableViewCell {

    // MARK: - Public

    // MARK: Inits

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: Functions

    func configure(with viewModel: SearchDisplayModelType) {
        searchDisplayModel = viewModel
        nameLabel.text = viewModel.title

         if let url = viewModel.imageURL {
             DispatchQueue.main.async {
                 self.backgroundImageView.af_setImage(withURL: url, placeholderImage: viewModel.placeholderImage)
             }
         } else {
             backgroundImageView.image = viewModel.placeholderImage
         }

        backgroundImageView.layer.masksToBounds = true
        addressLabel.text = viewModel.address
        configureGradientLayer()

        if let restaurantId = searchDisplayModel.id {
            let storedFavourites: [String] = userDefaults.value(forKey: Constants.storedFavouritesKey) as! [String]
            favouriteButton.imageView?.image = storedFavourites.contains(restaurantId) ? UIImage(named: "favourite_selected") : UIImage(named: "favourite_unselected")
        }

    }

    // MARK: - Private

    // MARK: - Properties

    fileprivate let userDefaults = UserDefaults.standard
    var searchDisplayModel: SearchDisplayModelType!

    // MARK: Outlets

    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var gradientView: UIView!
    @IBOutlet private weak var favouriteButton: UIButton!
    
    private func configureGradientLayer() {
        _ = gradientView.layer.sublayers?.popLast()
        let colours: [CGColor] = [UIColor.black.withAlphaComponent(0).cgColor, UIColor.black.withAlphaComponent(0.7).cgColor]
        let locations: [NSNumber] = [0.0, 1.0]

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colours
        gradientLayer.locations = locations
        gradientLayer.frame = bounds
        gradientView.layer.addSublayer(gradientLayer)
    }

    @IBAction func favouriteButtonTapped(_ sender: Any) {
        if let restaurantId = searchDisplayModel.id {

            var storedFavourites: [String] = userDefaults.value(forKey: Constants.storedFavouritesKey) as! [String]

            if storedFavourites.contains(restaurantId) {
                favouriteButton.setImage(UIImage(named: "favourite_unselected"), for: .normal)
                storedFavourites = storedFavourites.filter {
                    $0 != restaurantId
                }
            } else {
                favouriteButton.setImage(UIImage(named: "favourite_selected"), for: .normal)
                storedFavourites.append(restaurantId)
            }

            userDefaults.set(storedFavourites, forKey: Constants.storedFavouritesKey)
        }
    }
}
