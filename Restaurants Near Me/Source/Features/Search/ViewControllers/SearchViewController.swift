//
//  SearchViewController.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 30/9/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import Result
import UIKit

final class SearchViewController: UIViewController {

    var viewModel: SearchViewModelType!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        tableView.register(UINib(nibName: viewModel.cellIdentifier, bundle: nil), forCellReuseIdentifier: viewModel.cellIdentifier)
        updateRestaurants()
        searchTextField.delegate = self

        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard (_:)))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    // MARK: - Private

    // MARK: IBOutlets

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var noContentView: UIView!
    
    // MARK: Properties

    private var loadingIndicator: UIView!
    private var tapGestureRecognizer: UIGestureRecognizer!

    // MARK: Functions
    
    private func updateRestaurants() {

        if let text = searchTextField.text, viewModel.shouldSearchWith(searchString: text) {

            let spinnerView = displaySpinnerOn(view: self.view)
            viewModel.searchRestaurantsWith(keyword: text) { result in

                self.removeSpinner(spinner: spinnerView)
                switch result {
                case .success:
                    self.noContentView.isHidden = self.viewModel.allRestaurants.count > 0 ? true : false
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }

                case .failure:
                   let alertController = UIAlertController(title: self.viewModel.errorTitle, message: self.viewModel.errorMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: self.viewModel.errorDismissTitle, style: .cancel, handler: { _ in
                        self.dismiss(animated: true, completion: nil)
                    })

                    alertController.addAction(action)
                    self.present(alertController, animated: true)
                }

            }
        }
        
    }

    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        searchTextField.resignFirstResponder()
    }

   private func displaySpinnerOn(view: UIView) -> UIView {
        let spinnerView = UIView.init(frame: view.bounds)
        spinnerView.backgroundColor = .lightGray
        let activityIndicator = UIActivityIndicatorView.init(style: .whiteLarge)
        activityIndicator.startAnimating()
        activityIndicator.center = spinnerView.center

        let loadingTextLabel = UILabel()
        loadingTextLabel.textColor = UIColor.black
        loadingTextLabel.text = viewModel.loadingSpinnerTitle
        loadingTextLabel.sizeToFit()
        loadingTextLabel.center = CGPoint(x: activityIndicator.center.x, y: activityIndicator.center.y + 30)
        activityIndicator.addSubview(loadingTextLabel)

        DispatchQueue.main.async {
            spinnerView.addSubview(activityIndicator)
            spinnerView.addSubview(loadingTextLabel)
            view.addSubview(spinnerView)
        }

        return spinnerView
    }

    private func removeSpinner(spinner: UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }

}

// MARK: UITableViewDataSource

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRestaurants()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = viewModel.allRestaurants[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier, for: indexPath) as! SearchTableViewCell
        cell.configure(with: cellViewModel)
        return cell
    }
}

// MARK: UITableViewDelegate

extension SearchViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return CGFloat(viewModel.cellHeight)
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = viewModel.allRestaurants[indexPath.row].url, UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}

// MARK: UITextFieldDelegate

extension SearchViewController: UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        view.addGestureRecognizer(tapGestureRecognizer)
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        view.removeGestureRecognizer(tapGestureRecognizer)
            updateRestaurants()
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        view.removeGestureRecognizer(tapGestureRecognizer)
        return true
    }
}
