//
//  FavouritesViewController.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 31/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import Result
import UIKit

final class FavouritesViewController: UIViewController {

    var viewModel: FavouritesViewModelType!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        tableView.register(UINib(nibName: viewModel.cellIdentifier, bundle: nil), forCellReuseIdentifier: viewModel.cellIdentifier)
        noContentView.isHidden = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateRestaurants()
    }

    // MARK: - Private

    // MARK: IBOutlets

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var noContentView: UIView!

    // MARK: Properties

    private var loadingIndicator: UIView!

    // MARK: Functions
    
    private func updateRestaurants() {

        if viewModel.isUpdateRequired() {

            let spinnerView = displaySpinnerOn(view: self.view)
            viewModel.findFavourites { result in

                switch result {
                case .success:
                    self.noContentView.isHidden = self.viewModel.allRestaurants.count > 0 ? true : false
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    self.removeSpinner(spinner: spinnerView)

                case .failure:
                    self.removeSpinner(spinner: spinnerView)
                    let alertController = UIAlertController(title: self.viewModel.errorTitle, message: self.viewModel.errorMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: self.viewModel.errorDismissTitle, style: .cancel, handler: { _ in
                        self.dismiss(animated: true, completion: nil)
                    })

                    alertController.addAction(action)
                    self.present(alertController, animated: true)
                }

            }

        }
        
    }

   private func displaySpinnerOn(view: UIView) -> UIView {
        let spinnerView = UIView.init(frame: view.bounds)
        spinnerView.backgroundColor = .lightGray
        let activityIndicator = UIActivityIndicatorView.init(style: .whiteLarge)
        activityIndicator.startAnimating()
        activityIndicator.center = spinnerView.center

        let loadingTextLabel = UILabel()
        loadingTextLabel.textColor = UIColor.black
        loadingTextLabel.text = viewModel.loadingSpinnerTitle
        loadingTextLabel.sizeToFit()
        loadingTextLabel.center = CGPoint(x: activityIndicator.center.x, y: activityIndicator.center.y + 30)
        activityIndicator.addSubview(loadingTextLabel)

        DispatchQueue.main.async {
            spinnerView.addSubview(activityIndicator)
            spinnerView.addSubview(loadingTextLabel)
            view.addSubview(spinnerView)
        }

        return spinnerView
    }

    private func removeSpinner(spinner: UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }

}

// MARK: UITableViewDataSource

extension FavouritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRestaurants()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = viewModel.allRestaurants[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier, for: indexPath) as! FavouritesTableViewCell
        cell.delegate = self
        cell.configure(with: cellViewModel, index: indexPath.row)
        return cell
    }
}

// MARK: UITableViewDelegate

extension FavouritesViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.cellHeight)
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = viewModel.allRestaurants[indexPath.row].url, UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}

// MARK: - FavouriteCellTappedDelegate

extension FavouritesViewController: FavouriteCellTappedDelegate {
    public func favouriteButtonTappedWith(index: Int, restaurantId: String) {
        for i in 0..<viewModel.allRestaurants.count {
            if restaurantId == viewModel.allRestaurants[i].id {
                 viewModel.removeFavouriteAt(index: index, restaurantId: restaurantId)
                tableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
                if viewModel.allRestaurants.isEmpty {
                    noContentView.isHidden = false
                }
                break
            }
        }
    }
}
