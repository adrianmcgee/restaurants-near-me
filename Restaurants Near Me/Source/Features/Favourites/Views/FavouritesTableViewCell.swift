//
//  FavouritesTableViewCell.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 31/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import AlamofireImage
import UIKit

class FavouritesTableViewCell: UITableViewCell {

    // MARK: - Public

    // MARK: Inits

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: Functions

    func configure(with viewModel: FavouritesDisplayModelType, index: Int) {
        favouritesDisplayModel = viewModel
        nameLabel.text = viewModel.title

         if let url = viewModel.imageURL {
             DispatchQueue.main.async {
                 self.backgroundImageView.af_setImage(withURL: url, placeholderImage: viewModel.placeholderImage)
             }
         } else {
             backgroundImageView.image = viewModel.placeholderImage
         }

        backgroundImageView.layer.masksToBounds = true
        addressLabel.text = viewModel.address
        configureGradientLayer()
        
        favouriteButton.tag = index
    }

    // MARK: - Private

    // MARK: - Properties

    fileprivate let userDefaults = UserDefaults.standard
    var favouritesDisplayModel: FavouritesDisplayModelType!
    var delegate: FavouriteCellTappedDelegate!

    // MARK: Outlets

    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var gradientView: UIView!
    @IBOutlet private weak var favouriteButton: UIButton!

    private func configureGradientLayer() {
        _ = gradientView.layer.sublayers?.popLast()
        let colours: [CGColor] = [UIColor.black.withAlphaComponent(0).cgColor, UIColor.black.withAlphaComponent(0.7).cgColor]
        let locations: [NSNumber] = [0.0, 1.0]

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colours
        gradientLayer.locations = locations
        gradientLayer.frame = bounds
        gradientView.layer.addSublayer(gradientLayer)
    }

    @IBAction func favouriteButtonTapped(_ sender: Any) {
        if let restaurantId = favouritesDisplayModel.id {
            let button = (sender as! UIButton)
            delegate.favouriteButtonTappedWith(index: button.tag, restaurantId: restaurantId)
        }
    }
}

// MARK: - FavouriteCellTappedDelegate Protocol

public protocol FavouriteCellTappedDelegate {
    func favouriteButtonTappedWith(index: Int, restaurantId: String)
}
