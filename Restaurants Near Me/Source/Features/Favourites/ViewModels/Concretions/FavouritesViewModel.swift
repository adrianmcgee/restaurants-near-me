//
//  FavouritesViewModel.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 31/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import Result
import SwiftyJSON

final class FavouritesViewModel: FavouritesViewModelType {

    init(client: RestaurantClientType,
         favouritesRouter: FavouritesRouterType) {

    self.restaurantClient = client
    self.router = favouritesRouter
    }

    // MARK: - FavouritesViewModelType conformance

    var errorTitle: String {
        return "Error"
    }

    var errorMessage: String {
        return "There was an error. Please try again later"
    }

    var errorDismissTitle: String {
        return "OK"
    }

    var loadingSpinnerTitle: String {
        return "LOADING FAVOURITES"
    }

    var cellIdentifier: String {
        return "FavouritesTableViewCell"
    }
    
    var cellHeight: Float {
        return 200
    }

    var restaurants: [Restaurant] = []
    var allRestaurants: [FavouritesDisplayModelType] = []
    fileprivate let userDefaults = UserDefaults.standard

    func findFavourites(completion: @escaping (Result<Void, HTTPError>) -> Void) {

        allRestaurants = []

        currentFavourites = []

        let storedFavourites: [String] = userDefaults.value(forKey: Constants.storedFavouritesKey) as! [String]

        if storedFavourites.count > 0 {
            for id in storedFavourites {
                restaurantClient.findRestaurantWith(id: id) { [unowned self] result in

                    switch result {
                    case .success(let restaurant):
                        self.allRestaurants.append(FavouritesDisplayModel(restaurant: restaurant))

                        if let id = FavouritesDisplayModel(restaurant: restaurant).id {
                           if !self.currentFavourites.contains(id) {
                               self.currentFavourites.append(id)
                           }
                        }

                        if id == storedFavourites.last {
                            completion(Result(value: ()))
                        }

                    case .failure(let error):
                        completion(Result(error: error))
                    }

                }
            }
        } else {
            completion(Result(value: ()))
        }

    }

    func numberOfRestaurants() -> Int {
        return allRestaurants.count > 10 ? 10 : allRestaurants.count
    }

    func removeFavouriteAt(index: Int, restaurantId: String) {
        for i in 0..<allRestaurants.count {
            if restaurantId == allRestaurants[i].id {
                allRestaurants.remove(at: i)
                break
            }
        }

        let storedFavourites: [String] = userDefaults.value(forKey: Constants.storedFavouritesKey) as! [String]
        userDefaults.set(storedFavourites.filter {$0 != restaurantId}, forKey: Constants.storedFavouritesKey)
        currentFavourites = storedFavourites.filter {$0 != restaurantId}
    }

    func isUpdateRequired() -> Bool {
        let storedFavourites: [String] = userDefaults.value(forKey: Constants.storedFavouritesKey) as! [String]
        return !(storedFavourites == currentFavourites)
    }

    // MARK: - Private

    private let restaurantClient: RestaurantClientType
    private let router: FavouritesRouterType
    private var currentFavourites: [String] = []

    private func updateContent(_ restaurantResponse: FindRestaurantResponse) {
        allRestaurants.append(FavouritesDisplayModel(restaurant: restaurantResponse))
    }

}

struct Constants {
    static let storedFavouritesKey = "storedFavourites"
}
