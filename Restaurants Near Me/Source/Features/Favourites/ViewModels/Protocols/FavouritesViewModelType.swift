//
//  FavouritesViewModelType.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 31/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import Result

protocol FavouritesViewModelType {
    var restaurants: [Restaurant] { get }
    var allRestaurants: [FavouritesDisplayModelType] { get set }
    var errorTitle: String { get }
    var errorMessage: String { get }
    var errorDismissTitle: String { get }
    var loadingSpinnerTitle: String { get }
    var cellIdentifier: String { get }
    var cellHeight: Float { get }

    func findFavourites(completion: @escaping (Result<Void, HTTPError>) -> Void)
    func numberOfRestaurants() -> Int
    func removeFavouriteAt(index: Int, restaurantId: String)
    func isUpdateRequired() -> Bool
    }
