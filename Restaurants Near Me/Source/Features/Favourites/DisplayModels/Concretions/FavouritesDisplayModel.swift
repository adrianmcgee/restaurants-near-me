//
//  FavouritesDisplayModel.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 31/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation
import UIKit

final class FavouritesDisplayModel: FavouritesDisplayModelType {
    
    init(restaurant: Restaurant) {
        self.restaurant = restaurant
    }

    // MARK: - SearchDisplayModelType conformance

    var id: String? {
        guard let id = restaurant.id else { return nil }
        return id
    }

    var title: String {
        return restaurant.name ?? ""
    }

    var imageURL: URL? {
        guard let urlPath = restaurant.imageURLPath else { return nil }

        return URL(string: urlPath)
    }

    var url: URL? {
        guard let urlPath = restaurant.urlPath else { return nil }

        return URL(string: urlPath)
    }

    var address: String {
        return  restaurant.location?.address ?? ""
    }

    var placeholderImage: UIImage {
        return #imageLiteral(resourceName: "PlaceHolderImage")
    }
    
    // MARK: - Private
    
    private let restaurant: Restaurant

}
