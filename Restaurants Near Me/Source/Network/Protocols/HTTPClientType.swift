//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//
import Foundation
import Result

public protocol HTTPClientType {

  func request(_ httpRequest: HTTPRequestType, response: @escaping (Result<HTTPResponse, HTTPError>) -> Void)

}

public enum HTTPMethod: String {
  case delete = "DELETE"
  case get = "GET"
  case post = "POST"
  case put = "PUT"
  case patch = "PATCH"

  var name: String {
    return self.rawValue
  }
}

public enum HTTPError: Error, Equatable {
  case client(statusCode: Int, data: Data?) //400 ... 499,
  case server(Int) //500 ... 599,
  case noNetwork
  case request
  case unknown

  public static func == (left: HTTPError, right: HTTPError) -> Bool {

    switch (left, right) {

    case (.client, .client):
      return true

    case (.server, .server):
      return true

    case (.noNetwork, .noNetwork):
      return true

    case (.request, .request):
      return true

    case (.unknown, .unknown):
      return true

    default:
      return false
    }
  }
}

public typealias HTTPHeader = [String: String]
public typealias HTTPParameters = [String: String]
public typealias HTTPResponse = Data
