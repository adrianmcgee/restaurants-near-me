//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//
public protocol HTTPRequestBuilderType {

  func addPath(_ path: String) -> HTTPRequestBuilder

  func addMethod(_ method: HTTPMethod) -> HTTPRequestBuilder

  func addHeader(key: String, value: String) -> HTTPRequestBuilder

  func addQueryParameter(key: String, value: String) -> HTTPRequestBuilder

  func build() -> HTTPRequestType

}
