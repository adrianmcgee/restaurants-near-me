//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//
public protocol HTTPRequestType {

  var urlPath: String { get }
  var httpMethod: HTTPMethod { get }
  var httpHeaders: HTTPHeader { get }
  var parameters: HTTPParameters? { get }

}
