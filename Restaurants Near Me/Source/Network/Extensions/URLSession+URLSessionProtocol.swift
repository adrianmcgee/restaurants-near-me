//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import Foundation

extension URLSession: URLSessionProtocol {

  public func data(
    with request: URLRequest,
    completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
    self.dataTask(with: request, completionHandler: completionHandler).resume()
  }
}
