//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//
public struct HTTPRequest: HTTPRequestType {

  // MARK: - HTTPRequestType Conformance

  public var urlPath: String
  public var httpMethod: HTTPMethod
  public var httpHeaders: HTTPHeader
  public var parameters: HTTPParameters?

}
