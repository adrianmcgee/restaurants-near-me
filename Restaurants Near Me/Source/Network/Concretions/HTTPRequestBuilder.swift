//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//
public final class HTTPRequestBuilder: HTTPRequestBuilderType {

  init(apiVersionKey: String, apiVersion: String) {
    headers[apiVersionKey] = apiVersion
    headers["Content-Type"] = "application/json"

    //default values
    path = ""
    method = .get
    isFirstQueryParameter = true
  }

  // MARK: - HTTPRequestBuilderType Conformance

  public func addPath(_ path: String) -> HTTPRequestBuilder {
    self.path = path

    return self
  }

  public func addMethod(_ method: HTTPMethod) -> HTTPRequestBuilder {
    self.method = method

    return self
  }

  public func addHeader(key: String, value: String) -> HTTPRequestBuilder {
    headers[key] = value

    return self
  }

  public func addQueryParameter(key: String, value: String) -> HTTPRequestBuilder {

    let escapedString = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)

    if isFirstQueryParameter {
      path = path + key + "=" + escapedString!
      isFirstQueryParameter = false
    } else {
      path = path + "&" + key + "=" + escapedString!
    }

    return self
  }

  public func build() -> HTTPRequestType {

    return HTTPRequest(urlPath: path,
                       httpMethod: method,
                       httpHeaders: headers,
                       parameters: parameters)

  }

  // MARK: - Private

  // MARK: Properties

  var path: String
  var method: HTTPMethod
  var headers: HTTPHeader = [:]
  var parameters: HTTPParameters?
  var isFirstQueryParameter: Bool

}
