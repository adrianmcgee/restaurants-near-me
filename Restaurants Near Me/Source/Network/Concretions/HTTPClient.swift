//
//  SearchClient.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 1/10/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//
import Foundation
import Result

public class HTTPClient: HTTPClientType {

  public init(
    with base: String,
    session: URLSessionProtocol = URLSession.shared) {
    self.base = base
    self.session = session
  }

  // MARK: - Conformance

  // MARK: HTTPClientType

  public func request(_ httpRequest: HTTPRequestType, response: @escaping (Result<HTTPResponse, HTTPError>) -> Void) {

    guard let urlRequest = self.urlRequest(httpRequest: httpRequest) else {
      response(.failure(.request))
      return
    }

    session.data(with: urlRequest) { (data, urlResponse, error) in
              DispatchQueue.main.async {
        response(self.processResult(data: data, urlResponse: urlResponse, error: error))
      }

    }

  }

  // MARK: - Private

  // MARK: Properties

  private let base: String
  private let session: URLSessionProtocol

  // MARK: Functions

  private func processResult(data: Data?, urlResponse: URLResponse?, error: Error?) -> (Result<HTTPResponse, HTTPError>) {

    if error != nil {

      if let error = error as? URLError, error.code == .notConnectedToInternet {
        return Result(error: HTTPError.noNetwork)
      }

      return Result(error: HTTPError.request)

    } else if let httpResponse = urlResponse as? HTTPURLResponse {

      switch httpResponse.statusCode {

      case 200 ... 399:
        return self.parseSuccess(data: data)

      case 400 ... 499:
        return self.parseClientError(httpResponse: httpResponse, data: data)

      case 500 ... 599:
        return self.parseServerError(httpResponse: httpResponse, data: data)

      default:
        return Result(error: HTTPError.unknown)

      }
    } else {
        return Result(error: HTTPError.unknown)
    }

  }

  private func parseSuccess(data: Data?) -> (Result<HTTPResponse, HTTPError>) {

    guard let data = data else {
      return Result(error: HTTPError.unknown)
    }

    return Result(value: data)

  }

  private func parseClientError(httpResponse: HTTPURLResponse, data: Data?) -> (Result<HTTPResponse, HTTPError>) {

    return Result(error: HTTPError.client(statusCode: httpResponse.statusCode, data: data))

  }

  private func parseServerError(httpResponse: HTTPURLResponse, data: Data?) -> (Result<HTTPResponse, HTTPError>) {

    return Result(error: HTTPError.server(httpResponse.statusCode))

  }

  private func url(httpRequest: HTTPRequestType) -> URL? {

    guard let url = URL(string: "\(base)/\(httpRequest.urlPath)") else {
      return nil
    }

    if let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) {

      var characterSet = CharacterSet.urlQueryAllowed
      characterSet.remove(charactersIn: "+-:")

      return urlComponents.url

    }

    return url

  }

  private func urlRequest(httpRequest: HTTPRequestType) -> URLRequest? {

    guard let url = self.url(httpRequest: httpRequest) else {
      return nil
    }

    var urlRequest = URLRequest(url: url)
    urlRequest.httpMethod = httpRequest.httpMethod.rawValue
    urlRequest.allHTTPHeaderFields = httpRequest.httpHeaders

    return urlRequest

  }

}
