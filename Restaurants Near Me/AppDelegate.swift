//
//  AppDelegate.swift
//  Restaurants Near Me
//
//  Created by Adrian.McGee on 28/9/18.
//  Copyright © 2018 Adrian.McGee. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let userDefaults = UserDefaults.standard
        if userDefaults.value(forKey: Constants.storedFavouritesKey) == nil {
            let storedFavourites: [String] = []
            userDefaults.set(storedFavourites, forKey: Constants.storedFavouritesKey)
        }
        presentInitialScreen()
        return true
    }

    fileprivate func presentInitialScreen() {
        let router = AppStartRouter(window: window)
        router.presentInitialScreen()
    }

}
